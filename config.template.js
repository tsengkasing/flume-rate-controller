module.exports = {
    host: '',
    port: 3306,
    user: '',
    password: '',
    database: '',
    table: '',
    column: '',
    kafkaHost: '',
    kafkaTopic: '',
};
