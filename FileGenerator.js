const fs = require('fs');
const readline = require('readline');

class FileGenerator {
    /**
     * Constructor
     * @param {string} filePath 文件路径
     * @param {number | function} numOfLinesEachRound 每次读取的函数
     */
    constructor(filePath, numOfLinesEachRound = 10) {
        this.filePath = filePath;
        /**
         * 行数生成器
         * @type {function}
         */
        this.numOfLineGen = typeof numOfLinesEachRound === 'function' ? numOfLinesEachRound : () => numOfLinesEachRound;
        this.init();
    }

    init() {
        /**
         * 缓冲区
         * @type {Array<string>}
         */
        this.buf = [];
        const fileStream = fs.createReadStream(this.filePath);
        this.rl = readline.createInterface({
            input: fileStream,
            crlfDelay: Infinity
        });
        this.rl.on('line', line => {
            this.buf.push(line);
            if (this.buf.length >= this.numOfLineGen()) {
                this.rl.pause();
            }
        });
        this.rl.on('close', () => {
            this.init();
        });
    }

    fetch() {
        this.rl.resume();
        return new Promise((resolve, reject) => {
            const handler = () => {
                resolve(this.buf);
            };
            this.rl.on('close', handler);
            resolve(this.buf);
            this.buf = [];
            this.rl.removeListener('close', handler);
        });
    }
}

module.exports = FileGenerator;
