package main

import (
    "fmt"
    "flag"
    "time"
    "strings"
    "strconv"
    "database/sql"
    _ "github.com/go-sql-driver/mysql"
    "github.com/Shopify/sarama"
)

var (
    brokers = flag.String("brokers", "student13:9092", "Kafka Brokers")
    topic   = flag.String("topic", "weibo", "Kafka Topic")
    numPerSec = flag.String("numPerSec", "10000", "Num of weibos per seconds")
)

func sendToKafka(producer sarama.SyncProducer, topic string, num int, ch chan string) {
    var weibos []string
    var text string
    count := 0
    for ;; {
        text = <-ch
        weibos = append(weibos, text)
        if len(weibos) >= num {
            count += num
            var kafkaMessages []*sarama.ProducerMessage
            select {
            case <- time.After(time.Second * 1):
                for _, weibo := range(weibos) {
                    kafkaMessages = append(kafkaMessages, &sarama.ProducerMessage{
                        Topic: topic,
                        Value: sarama.StringEncoder(weibo),
                    })
                }
                err := producer.SendMessages(kafkaMessages)
                if err != nil {
                    fmt.Println("[ERROR]" + err.Error())
                }
                fmt.Println(fmt.Sprintf("count: %d", count))
            }
            weibos = nil
        }
    }
}

func main() {
    flag.Parse()

    if *brokers == "" {
        fmt.Println("No Broker")
    }

    if *topic == "" {
        fmt.Println("No Topic")
    }

    if *numPerSec == "" {
        fmt.Println("No Number limit")
    }

    numPerSecInt, err := strconv.Atoi(*numPerSec)
    if err != nil {
        panic(err.Error())
    }

    ch := make(chan string, numPerSecInt * 2)
    kafkaConf := sarama.NewConfig()
    kafkaConf.Producer.Retry.Max = 1
    kafkaConf.Producer.RequiredAcks = sarama.WaitForAll
    kafkaConf.Producer.Return.Successes = true
    syncProducer, err := sarama.NewSyncProducer(strings.Split(*brokers, ","), kafkaConf)
    if err != nil {
        fmt.Println("Error creating producer" + err.Error())
    }
    go sendToKafka(syncProducer, *topic, numPerSecInt, ch)


    // read database
    db, err := sql.Open("mysql", "hduser:student@unix(/run/mysqld/mysqld.sock)/weibo")
    if err != nil {
       panic(err.Error())
    }
    defer db.Close()
    for {
        rows, err := db.Query("SELECT `text` FROM `weibo`")
        if err != nil {
           panic(err.Error())
        }
        defer rows.Close()

        var weiboText string
        var numOfLine uint32
        for ; rows.Next() ; {
            numOfLine++
            err := rows.Scan(&weiboText)
            if err != nil {
                continue
            }
            ch<- weiboText
        }
    }
}
