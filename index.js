/**
 * @fileoverview
 * 控制 flume 读取数据的速率
 */

const path = require('path');
const FileGenerator = require('./FileGenerator');
const SQLGenerator = require('./SQLGenerator');

/**
 * 主函数
 */
(async function main() {
    let [type, ...args] = process.argv.slice(2);
    let g;
    switch(type) {
    case 'file': {
        let [filePath, maxNumOfLines] = args;
        if (!filePath) throw new Error('Please specify file path');
        maxNumOfLines = maxNumOfLines | 10;
        g = new FileGenerator(
            path.resolve(filePath),
            () => Math.max(Math.ceil(Math.random() * maxNumOfLines), 10)
        );
    } break;
    default:
    case 'database': {
        let [maxNumOfLines] = args;
        maxNumOfLines = maxNumOfLines | 10;
        const option = require('./config');
        g = new SQLGenerator(
            option,
            () => Math.max(Math.ceil(Math.random() * maxNumOfLines), 10)
        );
    } break;
    }

    let lines;
    while (true) {
        lines = await g.fetch();
        print(lines);
        await wait(800);
    }
})();

/**
 * 输出函数
 * 使用此函数 print 的内容会被 flume 读取
 * @param {Array<string>} lines
 */
function print(lines) {
    lines.forEach(line => console.log(line));
}

/**
 * 等待
 * @param {number} milliseconds 毫秒
 */
function wait(milliseconds) {
    return new Promise(resolve => {
        setTimeout(resolve, milliseconds);
    });
}
