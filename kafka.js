/**
 * @fileoverview
 * Directly send message to kafka
 */

const { KafkaClient, Producer } = require('kafka-node');
const option = require('./config');
const SQLGenerator = require('./SQLGenerator');

(function main() {
    let [minNumOfLines, maxNumOfLines] = process.argv.slice(2);
    minNumOfLines = minNumOfLines | 10;
    maxNumOfLines = maxNumOfLines | minNumOfLines * 2;

    const client = new KafkaClient({kafkaHost: option.kafkaHost});
    const producer = new Producer(client);

    const g = new SQLGenerator(
        option,
        () => Math.max(Math.ceil(Math.random() * maxNumOfLines), minNumOfLines)
    );

    const processing = async function () {
        let lines;
        while (true) {
            lines = await g.fetch();
            producer.send([
                {
                    topic: option.kafkaTopic,
                    messages: lines,
                }
            ], (err, data) => {
                if (err) console.error(err);
                else console.log(data);
            });
            await wait(5000);
        }
    };

    producer.on('ready', () => {
        processing();
    });

    producer.on('error', (err) => {
        console.error(new Date().toLocaleString({}, { timeZone: 'Asia/Shanghai' }));
        console.error(err);
    });

})();

/**
 * 等待
 * @param {number} milliseconds 毫秒
 */
function wait(milliseconds) {
    return new Promise(resolve => {
        setTimeout(resolve, milliseconds);
    });
}
