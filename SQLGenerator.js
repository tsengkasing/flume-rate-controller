
const mysql = require('mysql');

class SQLGenerator {
    /**
     * Constructor
     * @param {{host: string, user: string, password: string, database: string, table: string, column: string}}
     * @param {number | function} numOfLinesEachRound 每次读取的函数
     */
    constructor({host, port, user, password, database, table, column}, numOfLinesEachRound = 10) {
        this.option = {
            host     : host,
            user     : user,
            port     : port,
            password : password,
            database : database
        };
        this.table = table;
        this.column = column;
        /**
         * 行数生成器
         * @type {function}
         */
        this.numOfLineGen = typeof numOfLinesEachRound === 'function' ? numOfLinesEachRound : () => numOfLinesEachRound;
        this.init();
    }

    init() {
        this.buf = [];
        this.connection = mysql.createConnection(this.option);
        this.connection.connect(err => {
            if (err) console.error(err);
        });
        const query = this.connection.query(`SELECT ${this.column} FROM ${this.table};`);
        query.on('result', row => {
            this.buf.push(row['text']);
            if (this.buf.length >= this.numOfLineGen()) {
                this.connection.pause();
            }
        });
        query.on('error', (err) => {
            console.error('query error');
            console.error(err);
            this.init();
        });
        query.on('end', () => this.init());
    }

    /**
     * @returns {Promise<Array<any>>}
     */
    fetch() {
        return new Promise((resolve, reject) => {
            resolve(this.buf);
            this.buf = [];
            this.connection.resume();
        });
    }
}

module.exports = SQLGenerator;
